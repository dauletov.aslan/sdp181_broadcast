package kz.astana.broadcastexample;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button sendBroadcast = findViewById(R.id.sendBroadcast);
        sendBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("kz.astana.broadcastexample.NEW_MESSAGE");
                sendBroadcast(intent);
            }
        });

        Button sendBroadcastNotification = findViewById(R.id.sendBroadcastNotification);
        sendBroadcastNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("kz.astana.broadcastexample.NEW_NOTIFICATION");
                intent.putExtra("Message", "Hello! I'm Vasya Pupkin!");
                sendBroadcast(intent);
            }
        });

        Button sendBroadcastService = findViewById(R.id.sendBroadcastService);
        sendBroadcastService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("kz.astana.broadcastexample.START_SERVICE");
                intent.putExtra("Time", 10);
                sendBroadcast(intent);
            }
        });

        MyDynamicReceiver receiver = new MyDynamicReceiver();

        Button registerBroadcast = findViewById(R.id.registerBroadcast);
        registerBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("kz.astana.broadcastexample.NEW_MESSAGE");
                intentFilter.addAction("kz.astana.broadcastexample.NEW_NOTIFICATION");
                intentFilter.addAction("kz.astana.broadcastexample.START_SERVICE");
                intentFilter.addAction("kz.astana.broadcastexample.HELLO");
                registerReceiver(receiver, intentFilter);
            }
        });

        Button unregisterBroadcast = findViewById(R.id.unregisterBroadcast);
        unregisterBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unregisterReceiver(receiver);
            }
        });

        Button startService = findViewById(R.id.startService);
        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyStartService.class);
                startService(intent);
            }
        });
    }
}