package kz.astana.broadcastexample;

import android.app.RemoteInput;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle results = RemoteInput.getResultsFromIntent(intent);
        if (results != null) {
            String text = results.getString("KEY_REMOTE_INPUT");
            Log.d("Hello", text);
        }

        int time = intent.getIntExtra("Time", 0);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= time; i++) {
                    try {
                        Log.d("Hello", "Count: " + i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}