package kz.astana.broadcastexample;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

public class MyDynamicReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "New message", Toast.LENGTH_LONG).show();
        Log.d("Hello", "Has extra: " + intent.hasExtra("Hello"));
        if (intent.hasExtra("Message")) {
            createNotification(context, intent.getStringExtra("Message"));
        } else if (intent.hasExtra("Time")) {
            startService(context, intent.getIntExtra("Time", 0));
        } else if (intent.hasExtra("Hello")) {
            Toast.makeText(context, intent.getStringExtra("Hello"), Toast.LENGTH_LONG).show();
        }
    }

    private void createNotification(Context context, String message) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("CHANNEL_ID", "My", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("My notification channel");
            notificationManager.createNotificationChannel(notificationChannel);

            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    context,
                    1000,
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            );

            Intent serviceIntent = new Intent(context, MyService.class);
            PendingIntent replyIntent = PendingIntent.getService(
                    context,
                    2000,
                    serviceIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            );

            RemoteInput remoteInput =
                    new RemoteInput.Builder("KEY_REMOTE_INPUT")
                            .setLabel("Enter some text")
                            .build();
            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(
                            R.drawable.ic_launcher_foreground,
                            "Answer",
                            replyIntent
                    )
                            .addRemoteInput(remoteInput)
                            .build();

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "CHANNEL_ID");
            builder.setContentTitle("New notification");
            builder.setContentText(message);
            builder.setSmallIcon(R.drawable.ic_launcher_foreground);
            builder.setContentIntent(pendingIntent);
            builder.setAutoCancel(true);
            builder.addAction(R.drawable.ic_launcher_foreground, "Reply", replyIntent);
            builder.addAction(action);

            notificationManager.notify(100, builder.build());
        }
    }

    private void startService(Context context, int time) {
        Intent intent = new Intent(context, MyService.class);
        intent.putExtra("Time", time);
        context.startService(intent);
    }
}
