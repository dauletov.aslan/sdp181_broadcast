package kz.astana.broadcastexample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyStartService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("Hello", "Start sleeping");
                    TimeUnit.SECONDS.sleep(5);
                    Log.d("Hello", "Waking up");
                    Intent i = new Intent();
                    i.setAction("kz.astana.broadcastexample.HELLO");
                    i.putExtra("Hello", "Hello World!");
                    sendBroadcast(i);
                    stopSelf();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}